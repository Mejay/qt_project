#include "mythread.h"


#include <QTextEdit>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QTime>


MyClient::MyClient(const QString& strHost, int nPort,  QWidget* pwgt) :
    QWidget(pwgt), m_nNextBlockSize(0) // создаем ПВГХТ и зануляем буфер))00)

{
    m_pTcpSocket = new QTcpSocket(this);
    m_pTcpSocket->connectToHost(strHost, nPort); // передаем при соедении хост и порт
    connect(m_pTcpSocket, SIGNAL(connected()), SLOT(slotConnected())); // подключился
    connect(m_pTcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));// готов читать информацию
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError)));// ошибки при подключении

    m_ptxtInfo  = new QTextEdit;
    m_ptxtInfo->setStyleSheet("background: green; color: #fff;");
    m_ptxtInput = new QLineEdit;

    connect(m_ptxtInput, SIGNAL(returnPressed()), this, SLOT(slotSendToServer()));
    m_ptxtInfo->setReadOnly(true); //

    QPushButton* pcmd = new QPushButton("&Send");
    connect(pcmd, SIGNAL(clicked()), SLOT(slotSendToServer()));

    //Layout setup
    QVBoxLayout* pvbxLayout = new QVBoxLayout;
    pvbxLayout->addWidget(new QLabel("<H1>Client</H1>"));
    pvbxLayout->addWidget(m_ptxtInfo);
    pvbxLayout->addWidget(m_ptxtInput);
    pvbxLayout->addWidget(pcmd);
    setLayout(pvbxLayout);
}

void MyClient::slotReadyRead()
{
    QDataStream in(m_pTcpSocket);
    in.setVersion(QDataStream::Qt_4_2);

     // базовая проверка (Для того чтобы можно было читать сообщение блоками)
    for (;;)
    {

        if (!m_nNextBlockSize)
        {
            if (m_pTcpSocket->bytesAvailable() < sizeof(quint16)) {
                break;
            }
            in >> m_nNextBlockSize;
        }

        if (m_pTcpSocket->bytesAvailable() < m_nNextBlockSize)
        {
            break;
        }

    //////////////////////////////////////////////////////

        in >> str; // считывание всех сообщений и статусов


        if(str == "GO:") // если приходит то
        {
           m_ptxtInput->setDisabled(false); // разблокируется место для ввода данных
            m_ptxtInfo->setStyleSheet("background: green; color: #fff;");
        } else if(str == "Invalid move"){// если приходит то
            m_ptxtInput->setDisabled(false);// разблокируется место для ввода данных
            m_ptxtInfo->setStyleSheet("background: green; color: #fff;");

        } else if (str == "error"){// если приходит то
            m_ptxtInput->setDisabled(true);// блокируется место для ввода данных
            m_ptxtInfo->setStyleSheet("background: red; color: #fff;");

        }

        if(str != "GO:" && str != "Invalid move" && str != "error"){ // просто не выводить статусы
             m_ptxtInfo->append(str);// вывод игровой информации

        }


        m_nNextBlockSize = 0;
    }

}

void MyClient::slotError(QAbstractSocket::SocketError err) // проверка на ошибки (Базовый элемент в tcp)
{

    QString strError =
        "Error: " + (err == QAbstractSocket::HostNotFoundError ?
                     "The host was not found." :
                     err == QAbstractSocket::RemoteHostClosedError ?
                     "The remote host is closed." :
                     err == QAbstractSocket::ConnectionRefusedError ?
                     "The connection was refused." :
                     QString(m_pTcpSocket->errorString())
                    );

    m_ptxtInfo->append(strError);// вывод инфы о ошибке
}

void MyClient::slotSendToServer()
{

// базовая проверка
        QByteArray  arrBlock;
        QDataStream out(&arrBlock, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_4_2);
        out << quint16(0) << m_ptxtInput->text();

        out.device()->seek(0);
        out << quint16(arrBlock.size() - sizeof(quint16));
// ****************************

        m_ptxtInput->setDisabled(true);// блокирует окно если все нормально отправленно и теперь ждём сообщений от второго игрока
m_ptxtInfo->setStyleSheet("background: red; color: #fff;");
        m_pTcpSocket->write(arrBlock); // отправка сообщения
        m_ptxtInput->setText("");


}

void MyClient::slotConnected()
{

    m_ptxtInfo->append("Connect player!!!"); // если законектился то выводит данное сообщение

}
