#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QWidget>
#include <QTcpSocket>

class QTextEdit;
class QLineEdit;

class MyClient : public QWidget {
Q_OBJECT
private:
    QTcpSocket* m_pTcpSocket;
    QTextEdit*  m_ptxtInfo;
    QLineEdit*  m_ptxtInput;
    quint16     m_nNextBlockSize;

    QString str;

public:
    MyClient(const QString& strHost, int nPort, QWidget* pwgt = 0) ; // хост порт

    void status(QString str);//

private slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError);
    void slotSendToServer();
    void slotConnected();
};

#endif // MYTHREAD_H
